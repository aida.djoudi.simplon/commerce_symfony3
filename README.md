- set DATABASE_URL in .env
- 
- Créer un fichier .env.local à la racine
- y coller DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7
- 
- remplacer db_user et db_password au besoin
- remplacer db_name par un le nom de la database


install:

- composer install
- npm install
- npm run build
- php bin/console doctrine:database:create
- php bin/console make:migration
- php bin/console doctrine:migration:migrate
- symfony server:start
