Brief Gestion commerce
======================

Le projet se déroulera sur 2 semaines en binôme. L'objectif est de faire un outil de gestion de commerce sous symfony avec une base mysql.

Durée: 2 semaines
Projet par binôme

# Objectifs:
* Découvrir Symfony (Controller, twig, formulaires, entités, router, sécurité)
* Reprendre un schéma de données existant et le modifier
* Créer les composants pour interagir, créer un back-end

# Compétences
* Compétence 5 : créer une base de données, niveau imiter
* Compétence 6 : développer les composants d'accès au données, niveau imiter
* Compétence 7 :  Développer la partie back-end d'une application web, niveau imiter

# Semaine 1

## Etape 1
Définir l'architecture du projet, notamment au routage et à l'interface de l'application, faire un wireframe pour la table "sales" et customer.

L'architecture devra définir :
* une navigation avec les entrées principales
* un système de navigation par type de données (les définir au préalable)

## Etape 2
* Intégrer la base de données jointe
* Créer les controllers et les vues (liste et détaillée)
* Gérer un CRUD sur chaque type de donnée

Bonus :
* Intégrer des outils de filtrage sur les vues principales
* Ajouter des formulaires croisés sur la table sales
* Calculer les sommes des ventes sur la tables sales en fonction du filtrage
* Ajouter une recherche par nom/prénom sur la vue customer

# Semaine 2
## Etape 3 : modification du schéma de données
* Ajouter les entités productOptions, ProductOptionValue, ProductVariant

## Etape 4 :
Ajouter une entité user et protéger l'accès au backend par une gestion de mot depasse (make:auth)

## Bonus
* Faire un dashboard récapitulant la consultation des ventes
* Faire la partie publique qui liste les produits disponibles pour les clients




