<?php

namespace App\Controller;

use App\Entity\SaleProduct;
use App\Form\SaleProductType;
use App\Repository\SaleProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/sale/product")
 */
class SaleProductController extends AbstractController
{
    /**
     * @Route("/", name="sale_product_index", methods={"GET"})
     */
    public function index(SaleProductRepository $saleProductRepository): Response
    {
        return $this->render('sale_product/index.html.twig', [
            'sale_products' => $saleProductRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="sale_product_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $saleProduct = new SaleProduct();
        $form = $this->createForm(SaleProductType::class, $saleProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($saleProduct);
            $entityManager->flush();

            return $this->redirectToRoute('sale_product_index');
        }

        return $this->render('sale_product/new.html.twig', [
            'sale_product' => $saleProduct,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sale_product_show", methods={"GET"})
     */
    public function show(SaleProduct $saleProduct): Response
    {
        return $this->render('sale_product/show.html.twig', [
            'sale_product' => $saleProduct,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="sale_product_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, SaleProduct $saleProduct): Response
    {
        $form = $this->createForm(SaleProductType::class, $saleProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sale_product_index');
        }

        return $this->render('sale_product/edit.html.twig', [
            'sale_product' => $saleProduct,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="sale_product_delete", methods={"DELETE"})
     */
    public function delete(Request $request, SaleProduct $saleProduct): Response
    {
        if ($this->isCsrfTokenValid('delete'.$saleProduct->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($saleProduct);
            $entityManager->flush();
        }

        return $this->redirectToRoute('sale_product_index');
    }
}
