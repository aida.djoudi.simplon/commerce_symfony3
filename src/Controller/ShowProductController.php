<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Form\CustomerType;
use App\Repository\ProductRepository;
use App\Repository\CustomerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class ShowProductController extends AbstractController
{
    /**
     * @Route("/product", name="product_show_customer")
     */
    public function showproduct(ProductRepository $productRepository)
    {
        return $this->render('productshow.html.twig', [
            'productsshow' => $productRepository->findAll(),
            
        ]);
    }
}