<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="text")
     */
    private $supplier;

    /**
     * @ORM\Column(type="text")
     */
    private $webSiteSupplier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Tag", mappedBy="product")
     */
    private $tags;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SaleProduct", mappedBy="product", orphanRemoval=true)
     */
    private $saleProducts;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->saleProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSupplier(): ?string
    {
        return $this->supplier;
    }

    public function setSupplier(string $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getWebSiteSupplier(): ?string
    {
        return $this->webSiteSupplier;
    }

    public function setWebSiteSupplier(string $webSiteSupplier): self
    {
        $this->webSiteSupplier = $webSiteSupplier;

        return $this;
    }

    /**
     * @return Collection|Tag[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
            $tag->setProduct($this);
        }

        return $this;
    }

    public function removeTag(Tag $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
            // set the owning side to null (unless already changed)
            if ($tag->getProduct() === $this) {
                $tag->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SaleProduct[]
     */
    public function getSaleProducts(): Collection
    {
        return $this->saleProducts;
    }

    public function addSaleProduct(SaleProduct $saleProduct): self
    {
        if (!$this->saleProducts->contains($saleProduct)) {
            $this->saleProducts[] = $saleProduct;
            $saleProduct->setProduct($this);
        }

        return $this;
    }

    public function removeSaleProduct(SaleProduct $saleProduct): self
    {
        if ($this->saleProducts->contains($saleProduct)) {
            $this->saleProducts->removeElement($saleProduct);
            // set the owning side to null (unless already changed)
            if ($saleProduct->getProduct() === $this) {
                $saleProduct->setProduct(null);
            }
        }

        return $this;
        
    }
    public function __toString()
    {
        return $this->getName();
    }
    
        
}
