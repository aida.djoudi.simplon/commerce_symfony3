<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SaleRepository")
 */
class Sale
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetimetz")
     */
    private $dateOfpurchase;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Customer", inversedBy="sales")
     * @ORM\JoinColumn(nullable=false)
     */
    private $customer;

    /**
     * @ORM\Column(type="float")
     */
    private $amountSales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SaleProduct", mappedBy="sale", orphanRemoval=true)
     */
    private $saleProducts;

    public function __construct()
    {
        $this->saleProducts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateOfpurchase(): ?\DateTimeInterface
    {
        return $this->dateOfpurchase;
    }

    public function setDateOfpurchase(\DateTimeInterface $dateOfpurchase): self
    {
        $this->dateOfpurchase = $dateOfpurchase;

        return $this;
    }

    public function getCustomer(): ?Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): self
    {
        $this->customer = $customer;

        return $this;
    }

    public function getAmountSales(): ?string
    {
        return $this->amountSales;
    }

    public function setAmountSales(string $amountSales): self
    {
        $this->amountSales = $amountSales;

        return $this;
    }

    /**
     * @return Collection|SaleProduct[]
     */
    public function getSaleProducts(): Collection
    {
        return $this->saleProducts;
    }

    public function addSaleProduct(SaleProduct $saleProduct): self
    {
        if (!$this->saleProducts->contains($saleProduct)) {
            $this->saleProducts[] = $saleProduct;
            $saleProduct->setSale($this);
        }

        return $this;
    }

    public function removeSaleProduct(SaleProduct $saleProduct): self
    {
        if ($this->saleProducts->contains($saleProduct)) {
            $this->saleProducts->removeElement($saleProduct);
            // set the owning side to null (unless already changed)
            if ($saleProduct->getSale() === $this) {
                $saleProduct->setSale(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->getAmountSales();
    }

}
